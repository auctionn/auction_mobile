import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import filterReducer from './reducers/reducers';

import Content from './components/page/Contents';
import './common/css/base.css'
class App extends React.PureComponent {
	render() {
		const store = createStore(filterReducer);
		return (
			<Provider store={store}>
				<Router>
					<div className="App">
						<Content />
					</div>
				</Router>
			</Provider>
		)
	}
}

export default App;
