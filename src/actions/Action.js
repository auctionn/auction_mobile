import * as types from './ActionTypes';

export const click_filter_sort = () => ({
	type: types.CLICK_FILTER_SORT,
});

export const click_filter_kind = () => ({
	type: types.CLICK_FILTER_KIND,
});

export const click_filter_region = () => ({
	type: types.CLICK_FILTER_REGION,
});

export const click_filter_ing = () => ({
	type: types.CLICK_FILTER_ING,
});

export const click_filter_add = () => ({
	type: types.CLICK_FILTER_ADD,
});

export const click_detail_map = () => ({
	type: types.CLICK_DETAIL_MAP,
});
