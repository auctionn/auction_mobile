import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import $ from 'jquery'

import FilterHeader from '../page-module/FilterHeader';
import FilterContent from '../page-module/FilterContent';
import List from '../page-module/List';
import DetailSearch from '../page-module/DetailSearch';
import ListMap from '../page-module/ListMap';
import MyInfo from '../page-module/MyInfo';

import getList from '../../utils/utilMainRcords';

import logo from "../../images/logo.png";
import search from "../../images/i-search.png";
import map from "../../images/i-map.png";
import list from "../../images/i-list.png";

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			myInfoState: 'hide-content',
			mapState: 'hide-content',
			searchState: 'hide-content',
		}
		this.hideMyInfo = this.hideMyInfo.bind(this);
		this.hideMap = this.hideMap.bind(this);
		this.hideSearch = this.hideSearch.bind(this);
		this.onSearch = this.onSearch.bind(this);
	}
	componentDidMount() {
		$(function () {
			var lastScrollTop = 0,
				delta = 15;
			$(window).scroll(function (event) {
				var st = $(this).scrollTop();
				if (Math.abs(lastScrollTop - st) <= delta) return;
				if ((st > lastScrollTop) && (lastScrollTop > 0)) {
					$(".list .header").css("top", "-144px");
				} else {
					$(".list .header").css("top", "0px");
				}
				lastScrollTop = st;
			});
		});
		// detail page header 스크롤에 따라 반응함.
		$(window).scroll(function() {
			console.log("스크롤? jquery")
			var scroll = $(window).scrollTop();
			//console.log(scroll);
			if (scroll >= 50) {
				//console.log('a');
				$(".detail .header").addClass("navScroll");
			} else {
				//console.log('a');
				$(".detail .header").removeClass("navScroll");
			}
		});
	}
	onSearch(args) {
		getList(args).then((request) => {
			this.props.reLoad(request.data);
			console.log(request.body)
		});
	}
	hideMyInfo(params) {
		this.setState({
			...this.state,
			myInfoState: params,
		});
	}
	hideMap(params) {
		this.setState({
			...this.state,
			mapState: params,
		})
	}
	hideSearch(params) {
		this.setState({
			...this.state,
			searchState: params,
		})
	}
	handleMap = () => {
		// 지도 state(hide인지 아닌지)를 변화시키는 핸들러 함수입니다.
		const flag = this.state.mapState;
		if (flag === 'hide-content') {
			this.setState({
				...this.state,
				mapState: '',
			});
		} else {
			this.setState({
				...this.state,
				mapState: 'hide-content',
			});
		}
	}
	handleMyInfo = () => {
		// 내정보 state(hide인지 아닌지)를 변화시키는 핸들러 함수입니다.
		const flag = this.state.myInfoState;
		if (flag === 'hide-content') {
			this.setState({
				...this.state,
				myInfoState: '',
			});
		} else {
			this.setState({
				...this.state,
				myInfoState: 'hide-content',
			});
		}
	}
	handleSearch = () => {
		// 검색 state(hide인지 아닌지)를 변화시키는 핸들러 함수입니다.
		const flag = this.state.searchState;
		if (flag === 'hide-content') {
			this.setState({
				...this.state,
				searchState: '',
			});
		} else {
			this.setState({
				...this.state,
				searchState: 'hide-content',
			});
		}
	}

	render() {
		return (
			<div className="wrap">
				<div className="page main list">
					<div className="header">
						<div className="nav">
							<div className="logo">
								<li>
									<Link to="/">
										<img src={logo} alt="logo" />
									</Link>
								</li>
							</div>
							<div className="icon i-search" onClick={this.handleSearch}>
								<img src={search} alt="i-search" />
							</div>
							<div className="icon i-map" onClick={this.handleMap}>
								<img src={map} alt="i-map" />
							</div>
							<div className="icon i-list" onClick={this.handleMyInfo}>
								<img src={list} alt="i-list" />
							</div>
						</div>
						<FilterHeader />
					</div>
					<div className="content">
						<div className="product_list_wrap">
							<List
								records={this.props.records}
								foundRecord={this.props.foundRecord}
								currentPage={this.props.currentPage}
								onChange={this.onSearch}
							/>
						</div>
					</div>
				</div>
				<FilterContent onChange={this.onSearch} />
				<DetailSearch searchState={`${this.state.searchState}`} hideSearch={this.hideSearch} />
				<ListMap
					mapState={`${this.state.mapState}`}
					hideMap={this.hideMap}
					lat={this.props.lat}
					lng={this.props.lng}
					records={this.props.records}
					onChange={this.onSearch}
				/>
				<MyInfo myInfoState={`${this.state.myInfoState}`} hideMyInfo={this.hideMyInfo} />
			</div>
		);
	}
}

Home.propTypes = {
	records: PropTypes.array.isRequired,
	foundRecord: PropTypes.number.isRequired,
	currentPage: PropTypes.number.isRequired,
	reLoad: PropTypes.func.isRequired,
	lat: PropTypes.number.isRequired,
	lng: PropTypes.number.isRequired,
}

function reload(args) {
	return {
		type: '',
		records: args && args.records ? args.records : [],
		foundRecord : args && args.foundRecord ? args.foundRecord : 0,
		currentPage : args && args.currentPage ? args.currentPage : 1,
		hq : args && args.hq ? args.hq : 1,
	}
}

const listStateToProps = function (state) {
	return {
		records: state.listBox.records,
		foundRecord: state.listBox.foundRecord,
		currentPage: state.listBox.currentPage,
		lat: state.listBox.lat,
		lng: state.listBox.lng,
		hq: state.listBox.hq,
	};
};

const listDispatchToProps = dispatch => ({
	reLoad: args => dispatch(reload(args)),
});

Home = connect(listStateToProps, listDispatchToProps)(Home);


export default Home;
