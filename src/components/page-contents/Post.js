import React, { Component } from 'react';
import SimpleImageSlider from 'react-simple-image-slider';

import DefaultInfo from '../page-module/DefaultInfo';
import RealPriceInfo from '../page-module/RealPriceInfo';
import DetailMap from "../page-module/DetailMap";
import MyInfo from "../page-module/MyInfo";
import getDetail from '../../utils/utilPostRcords';

import close from '../../images/i-close-w.png';
import back from '../../images/i-back.png';
import like from '../../images/i-like.png';
import doc from '../../images/i-doc.png';
import list from '../../images/i-list.png';

class Post extends Component {
	constructor(props, { match }) {
		super(props, { match });
		this.state = {
			match: props.match,
			myInfoState: 'hide-content',
			docState: 'hide-content',
			popup: [
				{ id: "register", label: "건물등기", clicked: false },
				{ id: "appreport", label: "감정평가서", clicked: false },
				{ id: "statusreport", label: "현황조사서", clicked: false },
				{ id: "statement", label: "매각물건명세서", clicked: false },
				{ id: "housereading", label: "세대열람내역서", clicked: false },
				{ id: "estatelist", label: "부동산표시목록", clicked: false },
				{ id: "duedate", label: "기일내역", clicked: false },
				{ id: "deliverlist", label: "문건/송달내역", clicked: false },
				{ id: "eventlist", label: "사건내역", clicked: false },
				{ id: "elecmap", label: "전자지도", clicked: false },
				{ id: "eleclandmap", label: "전자지적도", clicked: false },
				{ id: "roadview", label: "로드뷰", clicked: false },
				{ id: "wholemap", label: "온나라지도", clicked: false },
				{ id: "additional", label: "부가정보", clicked: false },
			],
			record: [],
			gil: [],
			pyeongga_url: '',
			thumb: [],
			image: [],
			images: [{
				url: '',
			}],
			buildingPopup: [],
		};
		this.setData();
		this.hideDocument = this.hideDocument.bind(this);
		this.hideMyInfo = this.hideMyInfo.bind(this);
	}
	shouldComponentUpdate(nextProps, nextState) {
		return ((JSON.stringify(nextProps) !== JSON.stringify(this.props)) || (JSON.stringify(nextState) !== JSON.stringify(this.state)));
	}
	changeToHome = () => {
		this.props.history.goBack();
	}
	handleDocPopup = () => {
		// 팝업 state(hide인지 아닌지)를 변화시키는 핸들러 함수입니다.
		const flag = this.state.docState;
		if (flag === 'hide-content') {
			this.setState({
				...this.state,
				docState: '',
			});
		} else {
			this.setState({
				...this.state,
				docState: 'hide-content',
			});
		}
	}
	handleMyInfo = () => {
		// my information state(hide인지 아닌지)를 변화시키는 핸들러 함수입니다.
		const flag = this.state.myInfoState;
		if (flag === 'hide-content') {
			this.setState({
				...this.state,
				myInfoState: '',
			});
		} else {
			this.setState({
				...this.state,
				myInfoState: 'hide-content',
			});
		}
	}
	hideMyInfo(params) {
		this.setState({
			...this.state,
			myInfoState: params,
		});
	}
	hideDocument = () => { }
	setData() {
		getDetail(this.state.match.params.id).then((request) => {
			if ('data' in request) {
				const index = 0;
				const url = (request.data.records[0].pyeonggaseo.toString()).split('?');
				this.setState({
					record: request.data.records,
					gil: request.data.records[0].gil,
					thumb: request.data.records[0].title_thumb,
					image: request.data.records[0].thumbs,
					pyeongga_url: url[index],
				});
			}
			this.state.image.map((row) => {
				return (
					this.setState({
						images: this.state.images.concat({ url: row }),
					})
				);
			})
			this.state.images.shift();
		});
	}
	handleBuildingPush = (data) => {
		this.setState({
			// data를 list에 추가하여 새로운 list를 만든다.
			buildingPopup: this.state.buildingPopup.concat(data),
		});
	}
	render() {
		const popupMenu = this.state.popup.map((data, idx) => {
			return (
				<li id={data.id} idx={idx} key={data.id}>{data.label}</li>
			);
		})
		return (
			<div className="wrap">
				<div className="page main detail">
					<div className="header">
						<div className="nav">
							<div className="icon i-back" onClick={this.changeToHome}></div>
							<div className="icon i-like"></div>
							<div className="icon i-doc" onClick={this.handleDocPopup}></div>
							<div className="icon i-list" id="nonPopup" onClick={this.handleMyInfo}></div>
						</div>
					</div>
					<div className="content">
						<div className="img_slide">
							<SimpleImageSlider width={320} height={243} images={this.state.images} alt="img_slide" />
							<div className="bg"></div>
						</div>
						<div className="tab_wrap">
							<div className="tab_menu">
								<div className="tab">
									<input type="radio"
										   name="tab_menu"
										   id="tab_01"
										   className="default_menu"
										   defaultChecked="checked"
									/>
										<label htmlFor="tab_01"><span>기본 정보</span>
										</label>
									<input type="radio" name="tab_menu" id="tab_02" className="real_price_menu"/>
										<label htmlFor="tab_02"><span>실거래가 정보</span>
										</label>
										<div className="tab_contents default_info">
											<DefaultInfo record={this.state.record} gil={this.state.gil}/>
										</div>
										<div className="tab_contents real_price_menu">
											<RealPriceInfo record={this.state.record} gil={this.state.gil} match={this.props.match} />
										</div>
								</div>
							</div>
						</div>
					</div>
					<DetailMap
						postid={this.state.match.params.id}
						courtData={this.state.record}
						buildingPushData={this.handleBuildingPush}
					/>
				</div>
				<DetailMap />
				<div className={`layer-popup doc-section ${this.state.docState}`}>
					<div className="popup_header">
						<span>관련 문서 보기</span>
						<span className="icon i-close" onClick={this.handleDocPopup}>
							<img src={close} alt="i-close" />
        			    </span>
					</div>
					<div className="layer-containers">
						<div className="inner">
							<div className="box">
								<div className="doc-list">
									<ul>
										{popupMenu}
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<MyInfo myInfoState={`${this.state.myInfoState}`} hideMyInfo={this.hideMyInfo}/>
			</div>
		);
	}
}

export default Post;
