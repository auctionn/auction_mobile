import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from "../../actions/Action";

class DefaultInfo extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return ((JSON.stringify(nextProps) !== JSON.stringify(this.props)) || (JSON.stringify(nextState) !== JSON.stringify(this.state)));
	}
	handleMap = () => {
		this.props.onClickDetailMap();
	}
	render() {
		return (
			<div>
				<div className="card">
					<div className="card-inner">
						<h2 className="card-title">주변 아파트 정보</h2>
						<button className="btn btn-md btn-full blue" onClick={this.handleMap}>주변 아파트 정보 보기
						</button>
					</div>
				</div>
				<div className="card">
					<div className="card-inner">
						{this.props.record.map((row) => {
							return (
								<div className="info-section" key={row.toString()}>
									<h2 className="num-title">사건번호 {row.ID}</h2>
									<div className="address"><span>[{row.type}]</span> {row.detail_address}</div>
									<div className="court"><strong>법원</strong> {row.bubwon}</div>
									<div className="date"><strong>매각기일</strong> {row.gildata} <span className="badge red">입찰 {row.gildata ? row.gil_dday : '정보 없음'}</span></div>
									<div className="table">
										<table>
											<tbody>
											<tr>
												<th>대지권</th>
												<td>{row.platArea ? row.platArea : '정보없음'} ({row.platArea ? row.platArea * 0.3025 : '??'}평)</td>
											</tr>
											<tr>
												<th>건물면적</th>
												<td>{row.archArea} ({row.archArea * 0.3025}평)</td>
											</tr>
											<tr>
												<th>감정가</th>
												<td>{row.Appraisal_price}</td>
											</tr>
											<tr>
												<th>최저가</th>
												<td className="bold red">({row.Appraisal_price_percent}%){row.Appraisal_min_price}</td>
											</tr>
											<tr>
												<th>보증금</th>
												<td>(10%) 24,150,000</td>
											</tr>
											<tr>
												<th>매각물건</th>
												<td>토지·건물 일괄매각</td>
											</tr>
											<tr>
												<th>매각물건</th>
												<td>토지·건물 일괄매각</td>
											</tr>
											<tr>
												<th>{row.user1_type}</th>
												<td>{row.user1_name}</td>
											</tr>
											<tr>
												<th>{row.user2_type}</th>
												<td>{row.user2_name}</td>
											</tr>
											<tr>
												<th>{row.user3_type}</th>
												<td>{row.user3_name}</td>
											</tr>
											<tr>
												<th>사건접수</th>
												<td>{row.jub_date}</td>
											</tr>
											<tr>
												<th>사건명</th>
												<td>{row.sa_title}</td>
											</tr>
											<tr>
												<th>입찰방법</th>
												<td>{row.auction_how}</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
							);
						})}
					</div>
				</div>
				<div className="card">
					<div className="card-inner">
						<h2 className="card-title">입찰 진행 내용</h2>
						<div className="table">
							<table>
								<thead>
								<tr>
									<th>구분</th>
									<th>입찰기일</th>
									<th>최저매각가격</th>
									<th>결과</th>
								</tr>
								</thead>
								<tbody>
								{this.props.gil.map((row) => {
									return (
										<tr key={row.toString()}>
											<td>{row.MulGeonBunHo}</td>
											<td>{row.Gil}</td>
											<td>{row.GamJeongPyeongGaAek}</td>
											<td>{row.GilGyeolGwa}</td>
										</tr>
									);
								})}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

DefaultInfo.propTypes ={
	record: PropTypes.array.isRequired,
	gil: PropTypes.array.isRequired,
};

// 해당 액션을 dispatch하는 함수를 만든 후 이를 props로 연결합니다.
let mapDispatchToProps = (dispatch) => ({
	onClickDetailMap: () => dispatch(actions.click_detail_map()),
});

DefaultInfo = connect(undefined, mapDispatchToProps)(DefaultInfo);

export default DefaultInfo;
