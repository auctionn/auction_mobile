import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import getBuildingData from '../../utils/utilSearchRcords';
import getCenter from '../../utils/utilGetCenter';

import close from '../../images/i-close-w.png';
import * as actions from "../../actions/Action";

class DetailMap extends Component {
	constructor(props) {
		super(props);
		this.state = {
			disAlert: 'show',
			buildingRecords: [],
			centerLng: '',
			centerLat: '',
		};
	}
	componentDidMount() {
		// console.log(this.state.centerLat, this.state.centerLng);
		this.daumMap = window.daum.maps;
		const el = document.getElementById('map');
		const level = 5;
		const zoomControl = new this.daumMap.ZoomControl();
		this.obj = this;
		// mapObject = this;
		this.markers = [];
		this.contents = [];
		this.mapCustomOverlay = new this.daumMap.CustomOverlay();
		// console.log('this.props.postid', this.props.postid);
		getCenter(this.props.postid).then((request) => {
			if ('data' in request) {
				this.setState({
					centerLat : request.data.records[0].lat,
					centerLng : request.data.records[0].lng,
				});
				// console.log(this.state.centerLng);
				const map = new this.daumMap.Map(el, {
					center: new this.daumMap.LatLng(this.state.centerLat, this.state.centerLng),
					level: level,
				});
				const clusterer = new this.daumMap.MarkerClusterer({
					map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
					averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
					minLevel: 6, // 클러스터 할 최소 지도 레벨
					markers: this.markers,
					gridSize: 60,
					disableClickZoom: true, // 클러스터 클릭 시 지도 확대 되지 않는다
					styles: [{
						width: '43px',
						height: '42px',
						background: '#1e47b0 no-repeat',
						borderRadius: '100%',
						color: '#fff',
						textAlign: 'center',
						lineHeight:'43px',
						fontSize: '15px',
						opacity: 0.87,
					}],
				});
				this.map = map;
				this.clusterer = clusterer;
				this.map.addControl(zoomControl, this.daumMap.ControlPosition.RIGHT);
				this.onDetailMapDataChange(this.getLatLng());
				this.daumMap.event.addListener(map, 'dragend', () => {
					this.removeMarker();
					this.mapCustomOverlay.setMap(null);
					this.args = this.getLatLng();
					this.onDetailMapDataChange(this.args);
				});
				this.daumMap.event.addListener(map, 'zoom_changed', () => {
					this.removeMarker();
					this.args = this.getLatLng();
					this.onDetailMapDataChange(this.args);
				});
			}
		});
	}
	getLatLng() {
		const bounds = this.map.getBounds();
		// 영역의 남서쪽 좌표를 얻어옵니다
		const swLatLng = bounds.getSouthWest();
		// 영역의 북동쪽 좌표를 얻어옵니다
		const neLatLng = bounds.getNorthEast();
		const message = `지도의 남서쪽 좌표는 ${swLatLng.getLat()}, ${swLatLng.getLng()} 이고 <br>
		북동쪽 좌표는 ${neLatLng.getLat()}, ${neLatLng.getLng()} 입니다`;
		const args = {
			sw_lat: swLatLng.getLat(),
			sw_lng: swLatLng.getLng(),
			ne_lat: neLatLng.getLat(),
			ne_lng: neLatLng.getLng(),
			message: message,
		};
		return args;
	}
	onDetailMapDataChange(params) {
		getBuildingData(params).then((request) => {
			if ('data' in request) {
				this.setState({
					buildingRecords: request.data.records,
				});
			}
		});
	}
	addMarker(row, count) {
		const content = document.createElement('div');
		content.className = 'overlay_info';
		content.setAttribute('record-id', row.ID);
		// console.log('in addmarker', row); // current marker와는 다른 row 정보가 들어오고 있다.
		const minPrice = row.Appraisal_min_price ? this.convertToPrice(row.price[0]) : '정보 없음';
		// console.log(row);
		const buildingHide = count === 1 ? 'hide' : '';
		content.innerHTML = `<div class='post-marker'>
							<div class="kind">
								<span>${row.etcPurps}</span>
							</div>
							<div class='average-price'>
								${minPrice}
							</div>
							<div class=${buildingHide}>
								<div class='counting'}>같은 위치 물건
									<span>${count - 1}</span>건
								</div>
							</div>
						</div>`;
		// 커스텀 오버레이가 표시될 위치입니다
		const po = new this.daumMap.LatLng(row.lat, row.lng);
		// 커스텀 오버레이를 생성합니다
		const mapCustomOverlay = new this.daumMap.CustomOverlay({
			position: po,
			content: content,
			// xAnchor: 0.5, // 커스텀 오버레이의 x축 위치입니다. 1에 가까울수록 왼쪽에 위치합니다. 기본값은 0.5 입니다
			// yAnchor: 1.1 // 커스텀 오버레이의 y축 위치입니다. 1에 가까울수록 위쪽에 위치합니다. 기본값은 0.5 입니다
		});
		// 커스텀 오버레이를 지도에 표시합니다
		mapCustomOverlay.setMap(this.map);
		this.markers.push(mapCustomOverlay);
		this.contents.push(content);
		content.addEventListener('click', () => {
			this.map.panTo(po);
			this.props.buildingPushData(row);
		});
	}
	addCurrentMarker(row) {
		const content = document.createElement('div');
		const minPrice = row.Appraisal_min_price ? this.convertToPrice(row.price[0]) : '정보 없음';
		content.className = 'overlay_info';
		content.setAttribute('record-id', row.ID);
		// console.log(row);
		content.innerHTML = `<div class='current-marker'>
							<div class="kind">
								<span>현재물건</span>
							</div>
							<div class='average-price'>
								${minPrice}
							</div>
						</div>`;
		// 커스텀 오버레이가 표시될 위치입니다
		const po = new this.daumMap.LatLng(row.lat, row.lng);
		// 커스텀 오버레이를 생성합니다
		const mapCustomOverlay = new this.daumMap.CustomOverlay({
			position: po,
			content: content,
			// xAnchor: 0.5, // 커스텀 오버레이의 x축 위치입니다. 1에 가까울수록 왼쪽에 위치합니다. 기본값은 0.5 입니다
			// yAnchor: 1.1 // 커스텀 오버레이의 y축 위치입니다. 1에 가까울수록 위쪽에 위치합니다. 기본값은 0.5 입니다
		});
		// 커스텀 오버레이를 지도에 표시합니다
		mapCustomOverlay.setMap(this.map);
		this.markers.push(mapCustomOverlay);
		this.contents.push(content);
		this.addEventHandle(content, 'click', this.onMouseDown);
	}
	convertToPrice(price) {
		let minPriceStr = '';
		const intPriceHundred = Math.floor(price / 1000000) % 10;
		const intPriceThousand = Math.floor(price / 10000000) % 10;
		const intPriceHundredMillion = Math.floor(price / 100000000);
		const chun = intPriceThousand === 0 ? '' : `${intPriceThousand}천`;
		const baek = intPriceHundred === 0 ? '' : `${intPriceHundred}백`;
		// console.log(price);
		// console.log(`${intPriceHundredMillion}억${intPriceThousand}천${intPriceHundred}백`);
		minPriceStr = price >= 100000000 ? `${intPriceHundredMillion}억 ${chun}` : `${chun} ${baek}`;
		return minPriceStr;
	}
	render() {
		return (
			<div className={`layer-popup map_locate ${this.props.mapState}`}>
				<button className="close">
					<img src={close} alt="i-close-w" onClick={this.props.onClickDetailMap} />
				</button>
				<div className="layer-containers">
					<div className="inner">
						<div className="box">
							<div className="map-section" id="map">
								<div className="list-marker3">
									<div className="kind"><span>아파트</span></div>
									<div className="price">2억 3천</div>
								</div>
								<div className="alert">
									<div className="alert-desc">현재 물건 주변 아파트 실거래가 정보를 확인하세요.</div>
									<button type="button">
										<img src={close} alt="i-close-w"/>
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

let mapStateToProps = (state) => ({
	mapState: state.changeDetailMapType.mapState,
});

DetailMap.propTypes = {
	postid: PropTypes.string,
	courtData: PropTypes.array,
	buildingPushData: PropTypes.func,
};

// 해당 액션을 dispatch하는 함수를 만든 후 이를 props로 연결합니다.
let mapDispatchToProps = (dispatch) => ({
	onClickDetailMap: () => dispatch(actions.click_detail_map()),
});

DetailMap = connect(mapStateToProps, mapDispatchToProps)(DetailMap);

export default DetailMap;
