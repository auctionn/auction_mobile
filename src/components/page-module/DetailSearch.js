import React, { Component } from 'react';

import close from '../../images/i-close-w.png';
import search from '../../images/i-search-b.png';

class DetailSearch extends Component {
	handleSearchState = () => {
		this.props.hideSearch('hide-content');
	}
	render() {
		return(
			<div className={`layer-popup search-section ${this.props.searchState}`}>
				<div className="close blue" onClick={this.handleSearchState}>
					<img src={close} alt="i-close-w" />
				</div>
				<div className="layer-containers">
					<div className="inner">
						<div className="box">
							<div className="search">
								<input type="text" placeholder="검색어를 입력해 주" />
									<img src={search} alt="i-search-b" />
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default DetailSearch;