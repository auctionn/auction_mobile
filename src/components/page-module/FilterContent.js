import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../../actions/Action';

import CheckBox from './CheckBox';

import back from "../../images/i-back.png";

class FilterContent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectValue: 'non-select',
			numValue: 'non-select',
			kindAllChecked: true,
			houseAllChecked: true,
			commerceAllChecked: true,
			landAllChecked: true,
			trafficAllChecked: true,
			exceptraAllChecked: true,
			ingAllChecked: true,
			specialAllChecked: true,
			specialaddallChecked: true,
			procedureAllChecked: true,
			house: [
				{
					id: 'apt',
					label: '아파트',
					checked: true,
				},
				{
					id: 'housing',
					label: '단독주택',
					checked: true,
				},
				{
					id: 'neighborhoodhouse',
					label: '연립주택',
					checked: true,
				},
				{
					id: 'room',
					label: '다가구',
					checked: true,
				},
				{
					id: 'villa',
					label: '빌라',
					checked: true,
				},
			],
			commerce: [
				{
					id: 'mall',
					label: '상가',
					checked: true,
				},
				{
					id: 'facility',
					label: '근린시설',
					checked: true,
				},
				{
					id: 'officetel',
					label: '오피스텔',
					checked: true,
				},
				{
					id: 'office',
					label: '사무실',
					checked: true,
				},
				{
					id: 'storage',
					label: '창고',
					checked: true,
				},
				{
					id: 'factory',
					label: '공장',
					checked: true,
				},
				{
					id: 'apttypefactory',
					label: '아파트형 공장',
					checked: true,
				},
				{
					id: 'accommodation',
					label: '숙박시설',
					checked: true,
				},
				{
					id: 'etcaccommodation',
					label: '숙박(콘도 등)',
					checked: true,
				},
				{
					id: 'education',
					label: '교육시설',
					checked: true,
				},
				{
					id: 'religion',
					label: '종교시설',
					checked: true,
				},
				{
					id: 'farm',
					label: '농가관련시설',
					checked: true,
				},
				{
					id: 'medical',
					label: '의료시설',
					checked: true,
				},
				{
					id: 'dynamite',
					label: '주유소(위험물)',
					checked: true,
				},
				{
					id: 'bath',
					label: '목욕탕',
					checked: true,
				},
				{
					id: 'infirm',
					label: '노유자시설',
					checked: true,
				},
				{
					id: 'waste',
					label: '분뇨쓰레기처리',
					checked: true,
				},
				{
					id: 'car',
					label: '자동차관련시설',
					checked: true,
				},
				{
					id: 'funeral',
					label: '장례관련시설',
					checked: true,
				},
				{
					id: 'culture',
					label: '문화및집회시설',
					checked: true,
				},
			],
			land: [
				{
					id: 'site',
					label: '대지',
					checked: true,
				},
				{
					id: 'farmland',
					label: '농지',
					checked: true,
				},
				{
					id: 'forest',
					label: '임야',
					checked: true,
				},
				{
					id: 'hybrid',
					label: '잡종지',
					checked: true,
				},
				{
					id: 'orchard',
					label: '과수원',
					checked: true,
				},
				{
					id: 'road',
					label: '도로',
					checked: true,
				},
				{
					id: 'cemetery',
					label: '묘지',
					checked: true,
				},
				{
					id: 'ranch',
					label: '목장용지',
					checked: true,
				},
				{
					id: 'plant',
					label: '공장용지',
					checked: true,
				},
				{
					id: 'school',
					label: '학교용지',
					checked: true,
				},
				{
					id: 'depot',
					label: '창고용지',
					checked: true,
				},
				{
					id: 'physical',
					label: '체육용지',
					checked: true,
				},
				{
					id: 'religionpot',
					label: '종교용지',
					checked: true,
				},
				{
					id: 'etcpot',
					label: '기타용지',
					checked: true,
				},
				{
					id: 'ditch',
					label: '구거',
					checked: true,
				},
				{
					id: 'river',
					label: '하천',
					checked: true,
				},
				{
					id: 'oilpaper',
					label: '유지',
					checked: true,
				},
				{
					id: 'dike',
					label: '제방',
					checked: true,
				},
				{
					id: 'parking',
					label: '주차장',
					checked: true,
				},
			],
			traffic: [
				{
					id: 'sedan',
					label: '승용차',
					checked: true,
				},
				{
					id: 'bus',
					label: '버스',
					checked: true,
				},
				{
					id: 'truck',
					label: '화물차',
					checked: true,
				},
				{
					id: 'heavyequipment',
					label: '중장비',
					checked: true,
				},
				{
					id: 'ship',
					label: '선박',
					checked: true,
				},
			],
			exceptra: [
				{
					id: 'mining',
					label: '광업권',
					checked: true,
				},
				{
					id: 'fishery',
					label: '어업권',
					checked: true,
				},
				{
					id: 'salt',
					label: '염전',
					checked: true,
				},
				{
					id: 'fishfarm',
					label: '양어장',
					checked: true,
				},
				{
					id: 'etc',
					label: '기타',
					checked: true,
				},
			],
			ingFilter: [
				{
					id: 'progress',
					label: '진행물건',
					checked: true,
				},
				{
					id: 'new',
					label: '신건',
					checked: true,
				},
				{
					id: 'miscarriage',
					label: '유찰',
					checked: true,
				},
				{
					id: 'disposal',
					label: '매각',
					checked: true,
				},
				{
					id: 'permission',
					label: '매각허가',
					checked: true,
				},
				{
					id: 'paybalance',
					label: '잔금납부물건',
					checked: true,
				},
				{
					id: 'distribution',
					label: '배당기일종결',
					checked: true,
				},
				{
					id: 'etcprogress',
					label: '진행 외 물건',
					checked: true,
				},
				{
					id: 'nonprogress',
					label: '미진행',
					checked: true,
				},
				{
					id: 'change',
					label: '변경/연기',
					checked: true,
				},
				{
					id: 'nonpermission',
					label: '불허가/허가취소',
					checked: true,
				},
				{
					id: 'stop',
					label: '정지',
					checked: true,
				},
				{
					id: 'closeobject',
					label: '종국물건',
					checked: true,
				},
				{
					id: 'dismissal',
					label: '각하',
					checked: true,
				},
				{
					id: 'reject',
					label: '기각',
					checked: true,
				},
				{
					id: 'etcstatue',
					label: '기타',
					checked: true,
				},
				{
					id: 'transfer',
					label: '이송',
					checked: true,
				},
				{
					id: 'cancel',
					label: '취소',
					checked: true,
				},
				{
					id: 'drop',
					label: '취하',
					checked: true,
				},
				{
					id: 'terminate',
					label: '모사건 종결',
					checked: true,
				},
			],
			special: [
				{
					id: 'todaynew',
					label: '오늘 공고된 신건',
					checked: true,
				},
				{
					id: 'resale',
					label: '재매각 물건 검색',
					checked: true,
				},
				{
					id: 'halfsale',
					label: '반값 경매 물건',
					checked: true,
				},
				{
					id: 'afteroneyear',
					label: '감정일 1년 이상 물건',
					checked: true,
				},
				{
					id: 'violation',
					label: '위반 건축물',
					checked: true,
				},
				{
					id: 'setting',
					label: '전세권/임차권 설정',
					checked: true,
				},
			],
			specialadd: [
				{
					id: 'specialadd01',
					label: '선순위 전세권/임차권 설정',
					checked: true,
				},
				{
					id: 'specialadd02',
					label: '임차권 등기',
					checked: true,
				},
				{
					id: 'specialadd03',
					label: '전세권설정/임차권등기',
					checked: true,
				},
				{
					id: 'specialadd04',
					label: '말소기준등기보다 앞선 임차권',
					checked: true,
				},
				{
					id: 'specialadd05',
					label: '대항력 있는 임차인',
					checked: true,
				},
				{
					id: 'specialadd06',
					label: '선순위 가등기',
					checked: true,
				},
				{
					id: 'specialadd07',
					label: '선순위 가처분',
					checked: true,
				},
				{
					id: 'specialadd08',
					label: '예고등기',
					checked: true,
				},
				{
					id: 'specialadd09',
					label: '대지권미등기',
					checked: true,
				},
				{
					id: 'specialadd10',
					label: '토지별도등기 있는 물건',
					checked: true,
				},
				{
					id: 'specialadd11',
					label: '토지별도등기인수조건',
					checked: true,
				},
				{
					id: 'specialadd12',
					label: '건물만 입찰 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd13',
					label: '토지만 입찰 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd14',
					label: '지분입찰 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd15',
					label: '유치권 신고된 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd16',
					label: '법정지상권 여지 있는 물건 검색',
					checked: true,
				},
				{
					id: 'specialadd17',
					label: '분묘기지권 성립여지',
					checked: true,
				},
				{
					id: 'specialadd18',
					label: '유치권에 의한 형식적경매',
					checked: true,
				},
				{
					id: 'specialadd19',
					label: '공유물분할을 위한 형식적경매',
					checked: true,
				},
				{
					id: 'specialadd20',
					label: '청산을 위한 형식적경매',
					checked: true,
				},
				{
					id: 'specialadd21',
					label: '기타 형식적경매',
					checked: true,
				},
			],
			procedure: [
				{
					id: 'all',
					label: '전체보기',
					checked: true,
				},
				{
					id: 'random',
					label: '임의경매',
					checked: true,
				},
				{
					id: 'force',
					label: '강제경매',
					checked: true,
				},
			],
		}
		this.args = {
			hq: '',
			msq: '',
			mis: '',
			date: '',
			app: '',
			min: '',
			add: '',
			plot: '', // 대지면적
			area: '', // 건물면적
			num: '', // 지번범위
			orderby: '',
			appstart: '',
			append: '',
			minstart: '',
			minend: '',
			plotstart: '',
			plotend: '',
			areastart: '',
			areaend: '',
			numstart: '',
			numend: '',
		}
		this.handleInit = this.handleInit.bind(this);
		this.handleCheck = this.handleCheck.bind(this);
		this.changeInput = this.changeInput.bind(this);
		this.selectChange = this.selectChange.bind(this);
		this.selectAllChange = this.selectAllChange.bind(this);
	}
	handleInit(event) {
		if (event.target.id === 'kind') {
			this.setState({
				kindAllChecked: true,
			});
			let targetName = 0;
			let index = 0;
			const nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
			for (targetName = 0; targetName < nameArray.length; targetName += 1) {
				for (index = 0; index < this.state[nameArray[targetName]].length; index += 1) {
					if (this.state[nameArray[targetName]][index].checked === false) {
						this.changeCheckState(nameArray[targetName], index);
					}
				}
			}
		} else if (event.target.id === 'ing') {
			this.setState({
				ingFilterAllChecked: true,
			});
			let index = 0;
			for (index = 0; index < this.state.ingFilter.length; index += 1) {
				if (this.state.ingFilter[index].checked === false) {
					this.changeCheckState('ingFilter', index);
				}
			}
		} else if (event.target.id === 'add') {
			let index = 0;
			for (index = 0; index < this.state.special.length; index += 1) {
				if (this.state.special[index].checked === false) {
					this.changeCheckState('special', index);
				}
			}
			for (index = 0; index < this.state.procedure.length; index += 1) {
				if (this.state.procedure[index].checked === false) {
					this.changeCheckState('procedure', index);
				}
			}
			this.args.plotstart = '';
			this.args.plotend = '';
			this.args.areastart = '';
			this.args.areaend = '';
			this.args.numstart = '';
			this.args.numend = '';
			this.setState({
				numValue: 'non-select',
			});
			this.args.plot = '';
			this.args.area = '';
			this.args.num = '';
		}
	}
	changeCheckState = (targetName, index) => {
		const type = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
		const stateCopy = this.state[targetName];
		if (type.includes(targetName)) {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					kindAllChecked: false,
				});
				if (targetName === 'house') {
					this.setState({ houseAllChecked: false });
				} else if (targetName === 'commerce') {
					this.setState({ commerceAllChecked: false });
				} else if (targetName === 'land') {
					this.setState({ landAllChecked: false });
				} else if (targetName === 'traffic') {
					this.setState({ trafficAllChecked: false });
				} else if (targetName === 'exceptra') {
					this.setState({ exceptraAllChecked: false });
				}
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		} else if (targetName === 'ingFilter') {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					ingFilterAllChecked: false,
				});
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		} else if (targetName === 'special' || targetName === 'specialadd') {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					specialAllChecked: false,
				});
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		} else if (targetName === 'procedure') {
			if (this.state[targetName][index].checked === true) {
				stateCopy[index].checked = false;
				this.setState({
					[targetName]: stateCopy,
					procedureAllChecked: false,
				});
			} else {
				stateCopy[index].checked = true;
				this.setState({
					[targetName]: stateCopy,
				});
			}
		}
	}
	changeAllStatus = (targetIndex, flag) => {
		// 전체선택 시 아기 체크박스 변경되는 액션
		let index = 0;
		const nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra', 'ingFilter', 'special', 'specialadd', 'procedure'];
		if (flag) {
			for (index = 0; index < this.state[nameArray[targetIndex]].length; index += 1) {
				if (this.state[nameArray[targetIndex]][index].checked === false) {
					this.changeCheckState(nameArray[targetIndex], index);
				}
			}
		} else {
			for (index = 0; index < this.state[nameArray[targetIndex]].length; index += 1) {
				if (this.state[nameArray[targetIndex]][index].checked === true) {
					this.changeCheckState(nameArray[targetIndex], index);
				}
			}
		}
	}
	selectAllChange(event) {
		let targetIndex = 0;
		switch(event.target.id) {
			case "kindAllCheck":
				const kindFlag = !(this.state.kindAllChecked);
				this.setState({
					...this.state,
					kindAllChecked: kindFlag,
				});
				let targetName = 0;
				const nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
				for (targetName = 0; targetName < nameArray.length; targetName += 1) {
					this.changeAllStatus(targetName, kindFlag);
				}
				this.setState({
					...this.state,
					houseAllChecked: kindFlag,
					commerceAllChecked: kindFlag,
					landAllChecked: kindFlag,
					trafficAllChecked: kindFlag,
					exceptraAllChecked: kindFlag,
				});
				break;
			case "houseAllCheck":
				const houseFlag = !(this.state.houseAllChecked);
				this.setState({
					...this.state,
					houseAllChecked: houseFlag,
				});
				targetIndex = 0;
				this.changeAllStatus(targetIndex, houseFlag);
				break;
			case "commerceAllCheck":
				const commerceFlag = !(this.state.commerceAllChecked);
				this.setState({
					...this.state,
					commerceAllChecked: commerceFlag,
				});
				targetIndex = 1;
				this.changeAllStatus(targetIndex, commerceFlag);
				break;
			case "landAllCheck":
				const landFlag = !(this.state.landAllChecked);
				this.setState({
					...this.state,
					landAllChecked: landFlag,
				});
				targetIndex = 2;
				this.changeAllStatus(targetIndex, landFlag);
				break;
			case "trafficAllCheck":
				const trafficFlag = !(this.state.trafficAllChecked);
				this.setState({
					...this.state,
					trafficAllChecked: trafficFlag,
				});
				targetIndex = 3;
				this.changeAllStatus(targetIndex, trafficFlag);
				break;
			case "exceptraAllCheck":
				const exceptraFlag = !(this.state.exceptraAllChecked);
				this.setState({
					...this.state,
					exceptraAllChecked: exceptraFlag,
				});
				targetIndex = 4;
				this.changeAllStatus(targetIndex, exceptraFlag);
				break;
			case "ingFilterAllCheck":
				const ingFilterFlag = !(this.state.ingFilterAllChecked);
				this.setState({
					...this.state,
					ingFilterAllChecked: ingFilterFlag,
				});
				targetIndex = 5;
				this.changeAllStatus(targetIndex, ingFilterFlag);
				break;
			case "specialAllCheck":
				const specialFlag = !(this.state.specialAllChecked);
				this.setState({
					...this.state,
					specialAllChecked: specialFlag,
				});
				targetIndex = 6;
				this.changeAllStatus(targetIndex, specialFlag);
				targetIndex = 7;
				this.changeAllStatus(targetIndex, specialFlag);
				break;
			case "procedureAllCheck":
				const procedureFlag = !(this.state.procedureAllChecked);
				this.setState({
					...this.state,
					procedureAllChecked: procedureFlag,
				});
				targetIndex = 8;
				this.changeAllStatus(targetIndex, procedureFlag);
				break;
		}
	}
	selectChange(event) {
		if (event.target.id === 'sort') {
			this.setState({
				...this.state,
				selectValue: event.target.value,
			});
		} else if (event.target.id === 'num') {
			this.setState({
				...this.state,
				numValue: event.target.value,
			});
		}
	}
	handleCheck(event) {
		const targetName = event.target.name;
		const index = event.target.value;
		this.changeCheckState(targetName, index);
	}
	handleSave = (event) => {
		const args = [];
		let index = 0;
		let addStr = '';
		let targetName = 0;
		let nameArray = [];
		switch(event.target.id) {
			case "sortSave":
				this.args.orderby = this.state.selectValue;
				this.props.onChange(this.args);
				break;
			case "kindSave":
				nameArray = ['house', 'commerce', 'land', 'traffic', 'exceptra'];
				for (targetName = 0; targetName < nameArray.length; targetName += 1) {
					for (index = 0; index < this.state[nameArray[targetName]].length; index += 1) {
						if (this.state[nameArray[targetName]][index].checked === true) {
							addStr = `${this.state[nameArray[targetName]][index].id},`;
							this.str += addStr;
						}
					}
				}
				this.args.hq = this.str;
				this.props.onChange(this.args);
				this.str = '';
				break;
			case "regionSave":
				break;
			case "ingSave":
				for (index = 0; index < this.state.ingFilter.length; index += 1) {
					if (this.state.ingFilter[index].checked === true) {
						addStr = `${this.state.ingFilter[index].id},`;
						this.str += addStr;
					}
				}
				this.args.msq = this.str;
				this.props.onChange(this.args);
				this.str = '';
				break;
			case "addSave":
				nameArray = ['special', 'specialadd', 'procedure'];
				for (targetName = 0; targetName < nameArray.length; targetName += 1) {
					for (index = 0; index < this.state[nameArray[targetName]].length; index += 1) {
						if (this.state[nameArray[targetName]][index].checked === true) {
							addStr = `${this.state[nameArray[targetName]][index].id},`;
							this.str += addStr;
						}
					}
				}
				this.args.add = this.str;
				this.args.plot = `${this.args.plotstart},${this.args.plotend}`;
				this.args.area = `${this.args.areastart},${this.args.areaend}`;
				this.args.num = `${this.state.numValue},${this.args.numstart},${this.args.numend}`;
				this.props.onChange(this.args);
				this.str = '';
				break;
			default:
				break;
		}
	}
	changeInput(event) {
		event.preventDefault();
		this.setState({
			[event.target.id]: event.target.value,
		});
		if (event.target.name === 'start') {
			if (event.target.id === 'Ing') {
				this.args.misstart = event.target.value;
			} else if (event.target.id === 'plot') {
				this.args.plotstart = event.target.value;
			} else if (event.target.id === 'area') {
				this.args.areastart = event.target.value;
			} else if (event.target.id === 'num') {
				this.args.numstart = event.target.value;
			}
		}
		if (event.target.name === 'end') {
			if (event.target.id === 'Ing') {
				this.args.misend = event.target.value;
			} else if (event.target.id === 'plot') {
				this.args.plotend = event.target.value;
			} else if (event.target.id === 'area') {
				this.args.areaend = event.target.value;
			} else if (event.target.id === 'num') {
				this.args.numend = event.target.value;
			}
		}
	}
	render() {
		const houseItems = this.state.house.map((data, idx) => {
			const name = 'house';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const commerceItems = this.state.commerce.map((data, idx) => {
			const name = 'commerce';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const landItems = this.state.land.map((data, idx) => {
			const name = 'land';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const trafficItems = this.state.traffic.map((data, idx) => {
			const name = 'traffic';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const exceptraItems = this.state.exceptra.map((data, idx) => {
			const name = 'exceptra';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const ingFilterItems = this.state.ingFilter.map((data, idx) => {
			const name = 'ingFilter';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const specialItems = this.state.special.map((data, idx) => {
			const name = 'special';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />
			)
		})
		const procedureItems = this.state.procedure.map((data, idx) => {
			const name = 'procedure';
			return (
				<CheckBox id={data.id} idx={idx} key={data.id} name={name} onChange={this.handleCheck} checked={data.checked} label={data.label} />

			)
		})
		return (
			<div>
				<div className="정렬">
					<div className={`layer-popup product_type ${this.props.sortFlag}`}>
						<div className="popup_header">
							<div className="icon i-back" id="sort" onClick={this.props.onClickFilterSort}>
								<img src={back} alt="i-back" />
							</div>
							<div className="icon i-list">
								<span className="save" id="sortSave" onClick={this.handleSave}>저장</span>
							</div>
						<div className="inner">
							<div className="box">
								<div className="section">
									<div className="dropdown">
										<select className="select-dropdown" id="sort" onChange={this.selectChange}>
											<option value="non-select">선택안함</option>
											<option value="case-number-asc">사건번호↑</option>
											<option value="case-number-desc">사건번호↓</option>
											<option value="disposal-date-asc">매각기일↑</option>
											<option value="disposal-date-desc">매각기일↓</option>
											<option value="appraise-asc">감정가↑</option>
											<option value="appraise-desc">감정가↓</option>
											<option value="min-asc">최저가↑</option>
											<option value="min-desc">최저가↓</option>
											<option value="miscarriage-asc">유찰횟수↑</option>
											<option value="miscarriage-desc">유찰횟수↓</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
				<div className="물건종류">
					<div className={`layer-popup product_type ${this.props.kindFlag}`}>
						<div className="popup_header">
							<span className="icon i-back" onClick={this.props.onClickFilterKind}>
								<img src={back} alt=" " />
							</span>
							<span>물건종류</span>
							<span className="save" id="kindSave" onClick={this.handleSave}>저장</span>
						</div>
						<div className="layer-containers">
							<div className="inner">
								<div className="box">
									<div className="section">
										<div className="btn_wrap">
											<CheckBox id="kindAllCheck" onChange={this.selectAllChange} checked={this.state.kindAllChecked} label="전체선택" />
											<button className="btn btn-sm blue" id="kind">초기화</button>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>주거용</h3>
											<CheckBox id="houseAllCheck" onChange={this.selectAllChange} checked={this.state.houseAllChecked} label="주거용 전체선택" />
											<div className="col_multi_check">
												{houseItems}
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>상업용 및 업무용</h3>
											<CheckBox id="commerceAllCheck" onChange={this.selectAllChange} checked={this.state.commerceAllChecked} label="상업용 및 업무용 전체선택" />
											<div className="col_multi_check">
												{commerceItems}
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>토지</h3>
											<CheckBox id="landAllCheck" onChange={this.selectAllChange} checked={this.state.landAllChecked} label="토지 전체선택" />
											<div className="col_multi_check">
												{landItems}
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>차량 및 선박</h3>
											<CheckBox id="trafficAllCheck" onChange={this.selectAllChange} checked={this.state.trafficAllChecked} label="차량 및 선박 전체선택" />
											<div className="col_multi_check">
												{trafficItems}
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>기타</h3>
											<CheckBox id="exceptraAllCheck" onChange={this.selectAllChange} checked={this.state.exceptraAllChecked} label="기타 전체선택" />
											<div className="col_multi_check">
												{exceptraItems}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="지역">
					<div className={`layer-popup product_type ${this.props.regionFlag}`}>
					<div className="popup_header">
							<span className="icon i-back" onClick={this.props.onClickFilterRegion}>
								<img src={back} alt=" " />
							</span>
							<span>지역</span>
							<span className="save" id="regionSave" onClick={this.handleSave}>저장</span>
						</div>
					</div>
				</div>
				<div className="물건현황">
					<div className={`layer-popup product_type ${this.props.ingFlag}`}>
						<div className="popup_header">
							<span className="icon i-back" onClick={this.props.onClickFilterIng}>
								<img src={back} alt=" " />
							</span>
							<span>물건현황</span>
							<span className="save" id="ingSave" onClick={this.handleSave}>저장</span>
						</div>
						<div className="layer-containers">
							<div className="inner">
								<div className="box">
									<div className="section">
										<div className="btn_wrap">
											<CheckBox id="ingAllCheck" onChange={this.selectAllChange} checked={this.state.ingAllChecked} label="전체선택" />
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<div className="col_multi_check">
												{ingFilterItems}
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="필터 추가하기">
					<div className={`layer-popup product_type ${this.props.addFlag}`}>
						<div className="popup_header">
							<span className="icon i-back" onClick={this.props.onClickFilterAdd}>
								<img src={back} alt=" " />
							</span>
							<span>필터 추가하기</span>
							<span className="save" id="addSave" onClick={this.handleSave}>저장</span>
						</div>
						<div className="layer-containers">
							<div className="inner">
								<div className="box">
									<form>
									<div className="section">
										<div className="btn_wrap">
											<button type="reset" className="btn btn-sm blue btn-reset" id="add" onClick={this.handleInit}>초기화</button>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>대지면적</h3>
											<div className="input_content">
												<input type="text" name="start" id="plot" onChange={this.changeInput} />㎡ ~
												<input type="text" name="end" id="plot" onChange={this.changeInput} />㎡
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>건물면적</h3>
											<div className="input_content">
												<input type="text" name="start" id="area" onChange={this.changeInput} />㎡ ~
												<input type="text" name="end" id="area" onChange={this.changeInput} />㎡
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>지번 범위</h3>
											<div className="input_content">
												<select id="num" onChange={this.selectChange}>
													<option value="non-select">선택안함</option>
													<option value="all">전체</option>
													<option value="general">일반</option>
													<option value="mountain">산</option>
												</select>
												<input type="text" name="start" id="num" onChange={this.changeInput} /> ~
												<input type="text" name="end" id="num" onChange={this.changeInput} />
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>특수물건</h3>
											<CheckBox id="specialAllCheck" onChange={this.selectAllChange} checked={this.state.specialAllChecked} label="전체선택" />
											<div className="col_multi_check">
												{specialItems}
											</div>
										</div>
									</div>
									<div className="section">
										<div className="form_wrap">
											<h3>경매절차</h3>
											<CheckBox id="procedureAllCheck" onChange={this.selectAllChange} checked={this.state.procedureAllChecked} label="전체선택" />
											<div className="col_multi_check">
												{procedureItems}
											</div>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

FilterContent.propTypes = {
	onChange: PropTypes.func.isRequired,
};

// 스토어 안의 state 값을 props로 연결합니다.
let mapStateToProps = (state) => ({
    sortFlag: state.changeFilterType.sortFlag,
    kindFlag: state.changeFilterType.kindFlag,
    regionFlag: state.changeFilterType.regionFlag,
    ingFlag: state.changeFilterType.ingFlag,
    addFlag: state.changeFilterType.addFlag,
});

// 해당 액션을 dispatch하는 함수를 만든 후 이를 props로 연결합니다.
let mapDispatchToProps = (dispatch) => ({
    onClickFilterSort: () => dispatch(actions.click_filter_sort()),
    onClickFilterKind: () => dispatch(actions.click_filter_kind()),
    onClickFilterRegion: () => dispatch(actions.click_filter_region()),
    onClickFilterIng: () => dispatch(actions.click_filter_ing()),
    onClickFilterAdd: () => dispatch(actions.click_filter_add()),
});

FilterContent = connect(mapStateToProps, mapDispatchToProps)(FilterContent);

export default FilterContent;
