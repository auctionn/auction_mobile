import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions/Action';

class FilterHeader extends Component {
	render() {
		return(
			<div className="filter">
				<div className="filter_list">
					<ul>
						<li className="list_item">
							<button id="sort" onClick={this.props.onClickFilterSort}>정렬</button>
						</li>
						<li className="list_item">
							<button id="kind" onClick={this.props.onClickFilterKind}>물건종류</button>
						</li>
						<li className="list_item">
							<button id="region" onClick={this.props.onClickFilterRegion}>지역</button>
						</li>
						<li className="list_item">
							<button id="ing" onClick={this.props.onClickFilterIng}>물건현황</button>
						</li>
						<li className="list_item">
							<button id="add" onClick={this.props.onClickFilterAdd}>필터 추가하기</button>
						</li>
					</ul>
				</div>
			</div>
		);
	}
}


// 해당 액션을 dispatch하는 함수를 만든 후 이를 props로 연결합니다.
let mapDispatchToProps = (dispatch) => ({
    onClickFilterSort: () => dispatch(actions.click_filter_sort()),
    onClickFilterKind: () => dispatch(actions.click_filter_kind()),
    onClickFilterRegion: () => dispatch(actions.click_filter_region()),
    onClickFilterIng: () => dispatch(actions.click_filter_ing()),
    onClickFilterAdd: () => dispatch(actions.click_filter_add()),
});

FilterHeader = connect(undefined, mapDispatchToProps)(FilterHeader);

export default FilterHeader;
