import React, { Component } from 'react';
// import thumb from "../../images/thumb01.png";

import ListItem from '../page-module/ListItem';

class List extends Component {
	render() {
		return(
			<ul>
				{this.props.foundRecord === 0 ?
					<h5>
						<span>'검색한 키워드'</span>로 검색한 결과가 없습니다.
					</h5>
					:
					<div>
						{this.props.records.map((row, index) => {
							return <ListItem key={row.ID} data={row} reactkey={index} ref={`item${index}`} obj={this} />;
						})}
					</div>
				}
			</ul>
		);
	}
}

export default List;
