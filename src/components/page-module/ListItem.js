import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import thumb from "../../images/thumb01.png";

import PropTypes from 'prop-types';

class ListItem extends Component {
	render() {
		return (
			<li>
				<div record-id={this.props.data.ID}>
				<Link to={`/post/${this.props.data.ID}`} >
				<div className="thumb">
					<img src={this.props.data.record_thumb ? this.props.data.record_thumb : thumb} alt="thumb" />
					<span className="dday-bx">
						<em>{this.props.data.record_gil_dday < 0 ? `D${this.props.data.record_gil_dday}` : this.props.data.record_MulGeonStatus}</em>
					</span>
				</div>
				<div className="category">
					<span>{this.props.data.record_type}</span>
				</div>
				<div className="address">{this.props.data.record_address}</div>
				<div className="price_section price_appraise">
					<div className="badge">감정</div>
					<div className="price"><span>{this.props.data.record_Appraisal_price ? `${this.props.data.record_Appraisal_price}원` : '감정가 정보 없음'}</span></div>
				</div>
				<div className="price_section price_lowest">
					<div className="badge">최저</div>
					<div className="price"><span>{this.props.data.record_Appraisal_min_price ? `${this.props.data.record_Appraisal_min_price}원` : '최저가 정보 없음'}</span></div>
				</div>
				</Link>
				</div>
			</li>
		);
	}
}

ListItem.propTypes ={
	data: PropTypes.object.isRequired,
};

export default ListItem;