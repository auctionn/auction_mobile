import React, { Component } from 'react';

import close from '../../images/i-close-w.png';
import touch from '../../images/touch.png';
import { withRouter } from 'react-router-dom';

import ListMapItem from './ListMapItem';

class ListMap extends Component {
	constructor(props) {
		super(props);
		this.state = {
			touchState: 'map-list',
		};
	}
	handleMapState = () => {
		this.props.hideMap('hide-content');
	}
	componentDidMount() {
		this.daumMap = window.daum.maps;
		const el = document.getElementById('map');
		const level = 7; // 초기 지도 level
		const centerLat = 37.4981646; // 초기 지도 latitude(위도)
		const centerLng = 127.028307; // 초기 지도 longitude(경도)
		const map = new this.daumMap.Map(el, {
			center: new this.daumMap.LatLng(centerLat, centerLng),
			level: level,
		});
		const clusterer = new this.daumMap.MarkerClusterer({
			map: map, // 마커들을 클러스터로 관리하고 표시할 지도 객체
			averageCenter: true, // 클러스터에 포함된 마커들의 평균 위치를 클러스터 마커 위치로 설정
			minLevel: 7, // 클러스터 할 최소 지도 레벨
			markers: this.markers,
			gridSize: 60,
			clickable: true, // 클릭할 수 있음
			disableClickZoom: true, // 클러스터 클릭 시 지도 확대 되지 않는다
			styles: [{
				width: '43px',
				height: '42px',
				background: '#1e47b0 no-repeat',
				borderRadius: '100%',
				color: '#fff',
				textAlign: 'center',
				lineHeight:'43px',
				fontSize: '15px',
				opacity: 0.87,
			}],
		});
		this.map = map;
		this.markers = [];
		this.contents = [];
		this.infoMarker = [];
		this.infoContents = [];
		this.clusterer = clusterer;
		this.clusterer.setMinClusterSize(1);
		this.args = this.getLatLng();
		this.props.onChange(this.args);

		// 마커 클러스터 클릭 시 이벤트 발생
		this.daumMap.event.addListener(clusterer, 'clusterclick', (cluster) => {
			const clusteringLevel = this.map.getLevel() - 1;
			this.map.setLevel(clusteringLevel, { anchor: cluster.getCenter() });
		});

		// 드래그가 끝날 때 이벤트 발생
		this.daumMap.event.addListener(map, 'dragend', () => {
			this.clusterer.clear();
			this.args = this.getLatLng();
			this.props.onChange(this.args);
			console.log(this.args)
		});

		// 확대 수준이 변경될 때 이벤트 발생
		this.daumMap.event.addListener(map, 'zoom_changed', () => {
			this.clusterer.clear();
			this.args = this.getLatLng();
			this.props.onChange(this.args);
		});
	}
	addMarker(row) {
		const minPrice = row.record_price[2] ? this.convertToPrice(row.record_price[2]) : '정보 없음';
		const content = document.createElement('div');
		content.className = 'overlay_info';
		content.setAttribute('record-id', row.ID);
		content.innerHTML = `<div class="list-marker">
							<div class="kind">
								<span class="kind">${row.record_type}</span>
							</div>
							<div class="price-info">
								최저
								<div class="price">
									${minPrice}
								</ div>
							</div>
						</div>`;
		const position = new this.daumMap.LatLng(row.lat, row.lng);
		const mapCustomOverlay = new this.daumMap.CustomOverlay({
			position: position,
			content: content,
			clickable: true,
		});
		mapCustomOverlay.setMap(this.map);
		this.markers.push(mapCustomOverlay);
		this.contents.push(content);
		content.addEventListener('click', () => {
			this.props.history.push(`/post/${row.ID}`);
		});
	}
	getLatLng() {
		const subBounds = this.map.getBounds();
		const center = this.map.getCenter();
		const swLatLng = subBounds.getSouthWest();
		const neLatLng = subBounds.getNorthEast();
		const args = {
			center: center,
			sw_lat: swLatLng.getLat(),
			sw_lng: swLatLng.getLng(),
			ne_lat: neLatLng.getLat(),
			ne_lng: neLatLng.getLng(),
		};
		this.centerLat = (args.sw_lat + args.ne_lat) / 2;
		this.centerLng = (args.sw_lng + args.ne_lng) / 2;
		return args;
	}
	convertToPrice(price) {
		const intHundred = Math.floor(price / 1000000) % 10;
		const intThousand = Math.floor(price / 1000000) % 10;
		const intHundredMillion = Math.floor(price / 100000000);
		const Hundred = intHundred === 0 ? '' : `${intHundred}백`;
		const Thousand = intThousand === 0 ? '' : `${intThousand}천`;
		const minPrice = price >= 100000000 ? `${intHundredMillion}억 ${Thousand}` : `${Thousand} ${Hundred}`;
		return minPrice;
	}
	handleTouchStatus = () => {
		const flag = this.state.touchState === 'map-list' ? 'map-list-hide' : 'map-list';
		this.setState({
			...this.state,
			touchState: flag,
		});
	}
	render() {
		if (this.daumMap) {
			if (this.props.lat !== 0 && this.props.lng !== 0) {
				const coords = new this.daumMap.LatLng(this.props.lat, this.props.lng);
				this.map.setCenter(coords);
				this.args = this.getLatLng();
				this.props.onChange(this.args);
			} else {
				this.markers = [];
				this.clusterer.clear();
				this.props.records.forEach((row) => {
					this.addMarker(row);
				});
				this.clusterer.addMarkers(this.markers);
			}
		}
		return (
			<div className="리스트맵">
				<div className={`layer-popup map_locate ${this.props.mapState}`}>
					<button className="close" onClick={this.handleMapState}>
						<img src={close} alt=" " />
					</button>
					<div className="layer-containers">
						<div className="inner">
							<div className="box">
								<div className="map-section" id="map" />
								<div className={`${this.state.touchState}`}>
									<div className="touch-section">
										<div className="touch-icon" onClick={this.handleTouchStatus}>
											<img src={touch} alt="touch-icon" />
										</div>
										<div className="product_list_wrap">
											<ul>
												{this.props.records.map((row, index) => {
													return <ListMapItem key={row.ID} data={row} ref={`item${index}`} obj={this} />;
												})}
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(ListMap);
