import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import thumb from "../../images/thumb01.png";

class ListMapItem extends Component {
	render () {
		return (
			<li>
				<Link to={`post/${this.props.data.ID}`}>
				<div className="thumb_map">
					<img src={this.props.data.record_thumb ? this.props.data.record_thumb : thumb} alt="thumb_map" />
					<span className="dday-bx"><em>{this.props.record_gil_dday < 0 ? `D${this.props.data.record_gil_dday}` : this.props.data.record_MulGeonStatus}</em></span>
				</div>
				<div className="address">{this.props.data.record_address}</div>
				<div className="price_section price_appraise">
					<div className="badge">감정</div>
					<div className="price"><span>{this.props.data.record_Appraisal_price_percent ? `(${this.props.data.record_Appraisal_price_percent}%)` : '감정가 정보 없음'}</span></div>
				</div>
				<div className="price_section price_lowest">
					<div className="badge">최저</div>
					<div className="price"><span>{this.props.data.record_Appraisal_min_price ? `(${this.props.data.record_Appraisal_min_price})` : '최저가 정보 없음'}</span></div>
				</div>
				</Link>
			</li>
		);
	}
}

export default ListMapItem;