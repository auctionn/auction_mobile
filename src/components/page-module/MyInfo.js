import React, { Component } from 'react';
import PropTypes from 'prop-types';

import defaultProfile from '../../images/default-pf.png';

class MyInfo extends Component {
	handleMyInfoState = () => {
		this.props.hideMyInfo('hide-content');
	}
	render() {
		return(
			<div className={`layer-popup right-menu ${this.props.myInfoState}`} onClick={this.handleMyInfoState}>
				<div className="layer-containers">
				<div className="inner">
					<div className="box">
						<div className="profile">
							<div className="pf-thumb">
								<img src={defaultProfile} alt="default-pf" />
							</div>
							<div className="login">로그인 해주세요</div>
							<div className="signin">혹은 <span>회원가입</span> 하기</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		);
	}
}

MyInfo.propTypes = {
	hideMyInfo: PropTypes.func.isRequired,
};

export default MyInfo;