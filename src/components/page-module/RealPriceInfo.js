import React, { Component } from 'react';
import PropTypes from 'prop-types';
import getActualpriceData from '../../utils/utilActualRcords';
import getActualAvg from '../../utils/utilActualAvg';
import PostLineChart from "./PostLineChart";
import PostChart from './PostChart';

class RealPriceInfo extends Component {
	constructor(props, { match }) {
		super(props, { match });
		this.state = {
			sixAvg: 0,
			oneAvg: 0,
			prices: [],
			labels: [],
			m2s: [],
			maxPrice: 0,
			minPrice: 0,
			maemaeType: '매매',
			m2Val: 'All',
			listDrop: {
				type: 'hide',
				m2: 'hide',
			},
			actualPriceRecords: [],
			naverGroundUrl: '',
			chartTypeOn: {
				fiveyears: '01', // 5년
				tenyears: 'hide-content', // 10년
				sale: 'hide-content', // 전세/매매
				changerate: 'hide-content', // 증감률
				chartType: 'fiveyears',
			},
		};
	}
	componentWillMount() {
		const params = {
			id: this.props.match.params.id,
			years: this.state.chartTypeOn.chartType,
			type: this.state.maemaeType,
			address: '',
			buildnm: '',
			m2: this.state.m2Val,
		};
		this.getActualPrice(params);
		this.getActualAvg(params);
	}

	getActualAvg(params) {
		getActualAvg(params).then((request) => {
			if ('data' in request) {
				this.setState({
					oneAvg: Number.isInteger(request.data.records.oneavg) ? this.convertToPrice(request.data.records.oneavg) : 0,
					sixAvg: Number.isInteger(request.data.records.sixavg) ? this.convertToPrice(request.data.records.sixavg) : 0,
					naverGroundUrl: request.data.records.naver_ground_url,
				});
			}
		});
	}
	convertToPrice(price) {
		let minPriceStr = '';
		const intPriceHundred = Math.floor(price / 100) % 10;
		const intPriceThousand = Math.floor(price / 1000) % 10;
		const intPriceHundredMillion = Math.floor(price / 10000);
		const chun = intPriceThousand === 0 ? '' : `${intPriceThousand}천`;
		const baek = intPriceHundred === 0 ? '' : `${intPriceHundred}백`;
		minPriceStr = price >= 10000 ? `${intPriceHundredMillion}억 ${chun}` : `${chun} ${baek}`;
		return minPriceStr;
	}
	handleChart = (event) => {
		this.setState({
			chartTypeOn: {
				fiveyears: '',
				tenyears: '',
				sale: '',
				changerate: '',
			},
		});
		switch (event.target.value) {
			case "fiveyears":
				this.setState({
					chartTypeOn: {
						fiveyears: '',
						chartType: 'fiveyears',
					},
				});
				break;
			case "tenyears":
				this.setState({
					chartTypeOn: {
						tenyears: '',
						chartType: 'tenyears',
					},
				});
				break;
			case "sale":
				this.setState({
					chartTypeOn: {
						sale: '',
						chartType: 'sale',
					},
				});
				break;
			case "changerate":
				this.setState({
					chartTypeOn: {
						changerate: '',
						chartType: 'changerate',
					},
				});
				break;
			default:
				break;
		}
		const params = {
			id: this.props.match.params.id,
			years: event.target.value,
			type: this.state.maemaeType,
			address: '',
			buildnm: '',
			m2: this.state.m2Val,
		};
		this.getActualPrice(params);
	}
	handleMaeMaeType = (event) => {
		this.dropDownHide();
		this.setState({
			maemaeType: event.target.value,
		});
		const params = {
			id: this.props.match.params.id,
			years: this.state.chartTypeOn.chartType,
			type: this.state.maemaeType,
			address: '',
			buildnm: '',
			m2: this.state.m2Val,
		};
		this.getActualPrice(params);
	}
	reLoadActualpriceData() {
		this.setState({
			labels: [],
			prices: [],
			m2s: [],
		});
	}
	getActualPrice(params) {
		this.reLoadActualpriceData();
		getActualpriceData(params).then((request) => {
			if ('data' in request) {
				this.setState({
					actualPriceRecords: request.data.records,
				});
				this.state.actualPriceRecords.forEach((row) => {
					// console.info(row);
					this.setState({
						m2Val: params.m2,
						labels: this.state.labels.concat(row.yearsmonth),
						prices: this.state.prices.concat(row.price),
						m2s: this.state.m2s.concat(row.m2).filter(info => info !== row.m2),
					});
				});
				if (this.state.prices.length <= 0) {
					this.setState({
						labels: [0, 1],
						prices: [0, 0],
					});
				}
				const max = Math.max.apply(null, this.state.prices);
				const min = Math.min.apply(null, this.state.prices);
				this.setState({
					maxPrice: Number.isInteger(max) ? this.convertToPrice(max) : '',
					minPrice: Number.isInteger(min) ? this.convertToPrice(min) : '',
				});
			}
		});
	}
	handleM2Type = (event) => {
		this.dropDownHide();
		this.setState({
			m2Val: event.target.value,
		});
		const params = {
			id: this.props.match.params.id,
			years: this.state.chartTypeOn.chartType,
			type: event.target.value,
			address: '',
			buildnm: '',
			m2: event.target.value,
		};
		this.getActualPrice(params);
	}
	dropDownHide() {
		this.setState({
			listDrop: {
				type: 'hide',
				m2: 'hide',
			},
		});
	}
	render() {
		return (
			<div>
				<div className="card">
					<div className="card-inner">
						<div className="dropdown_wrap">
							<select className="item-sale" onChange={this.handleMaeMaeType}>
								<option value="매매">매매</option>
								<option value="전월세">전세</option>
							</select>
							<select className="item-measure" onChange={this.handleM2Type}>
								<option value="All">All</option>
								{this.state.m2s.map((innerRow) => {
									return (
										<option value={innerRow}>{innerRow} (m²)</option>
									)
								})}
							</select>
						</div>
						<div className={`price_wrap ${this.state.listDrop.type}`}>
							{this.props.record.map((row) => {
								return (
									<div key={row.ID}>
										<div className="price-group">
											<div className="title">6달 전 매매 실거래 평균</div>
											<div className="price">{this.state.sixAvg}</div>
											<div className="caption">국토교통부 평균 가격</div>
										</div>
										<div className="real-price-group">
											<div className="title">최근 매물 평균</div>
											<div className="price">{this.state.oneAvg}</div>
											<div className="caption">1개월 평균 가격</div>
										</div>
									</div>
								);
							})
							}
						</div>
						<div className="chart">
							<button className="chart_tab01" value="fiveyears" onClick={this.handleChart}>5년</button>
							<button className="chart_tab02" value="tenyears" onClick={this.handleChart}>10년</button>
							<button className="chart_tab03" vale="sale" onClick={this.handleChart}>전세/매매</button>
							<button className="chart_tab04" value="changerate" onClick={this.handleChart}>증감률</button>
							<div className={`chart_contents`}>
								<div className="chart-status">
									<span className="status-text">최고 {this.state.maxPrice} /</span>
									<span className="status-text">최저 {this.state.minPrice} /</span>
									<span className="status-text">(총 {this.state.actualPriceRecords.length}건)</span>
								</div>
								<PostLineChart prices={this.state.prices} labels={this.state.labels}/>
							</div>
						</div>
						<PostChart actualPriceRecords={this.state.actualPriceRecords} />
					</div>
				</div>
				<div className="card">
					<div className="card-inner">
						<div className="base-info">
							<ul>
								<li>공동주택</li>
								<li>8세대</li>
								<li>1개동</li>
								<li>최고 5층</li>
								<li>용적율 202.56%</li>
								<li>건폐율 56.75%</li>
								<li>총 주차 8대 가능</li>
								<li>개별난방(보류)</li>
								<li>도시가스(보류)</li>
								<li>내진설계대상(보류)</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

RealPriceInfo.propTypes = {
	match: PropTypes.object.isRequired,
	record: PropTypes.array.isRequired,
	gil: PropTypes.array.isRequired,
}

export default RealPriceInfo;