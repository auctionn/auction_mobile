import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from '../page-contents/Home';
import Post from '../page-contents/Post';

class Content extends Component {
	render() {
		return (
			<Router>
				<div id="main" className="container">
					<Switch>
						<Route exact path='/' component={Home}/>
						<Route path='/post/:id' component={Post}/>
					</Switch>
				</div>
			</Router>
		);
	}
}

export default Content;
