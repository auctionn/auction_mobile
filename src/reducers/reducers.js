import * as types from '../actions/ActionTypes';
import { combineReducers } from 'redux';

// 초기 상태
const initialFilterState = {
	sortFlag: 'hide-content',
	kindFlag: 'hide-content',
	regionFlag: 'hide-content',
	ingFlag: 'hide-content',
	addFlag: 'hide-content',
};

const initialMapState = {
	mapState: 'hide-content',
};

// reducer
function changeFilterType(state = initialFilterState, action) {
	switch (action.type) {
		case types.CLICK_FILTER_SORT:
			const sortFlagValue = state.sortFlag === 'hide-content' ? '' : 'hide-content';
			return {
				...state,
				sortFlag: sortFlagValue,
			};
		case types.CLICK_FILTER_KIND:
			const kindFlagValue = state.kindFlag === 'hide-content' ? '' : 'hide-content';
			return {
				...state,
				kindFlag: kindFlagValue,
			};
		case types.CLICK_FILTER_REGION:
			const regionFlagValue = state.regionFlag === 'hide-content' ? '' : 'hide-content';
			return {
				...state,
				regionFlag: regionFlagValue,
			};
		case types.CLICK_FILTER_ING:
			const ingFlagValue  = state.ingFlag === 'hide-content' ? '' : 'hide-content';
			return {
				...state,
				ingFlag: ingFlagValue,
			};
		case types.CLICK_FILTER_ADD:
			const addFlagValue = state.addFlag === 'hide-content' ? '' : 'hide-content';
			return {
				...state,
				addFlag: addFlagValue,
			};
		default:
			return state;
	}
};

function changeDetailMapType(state = initialMapState, action) {
	switch (action.type) {
		case types.CLICK_DETAIL_MAP:
			const mapStateValue = state.mapState === 'hide-content' ? '' : 'hide-content';
			return {
				...state,
				mapState: mapStateValue,
			};
		default:
			return state;
	}
}

const focusBox = (state = null, action) =>
	Object.assign({}, state, {
		focusID: action.focusID ? action.focusID : '0',
	});

const listBox = (state = null, action) =>
	Object.assign({}, state, {
		records: action.records ? action.records : [],
		currentPage: action.currentPage ? parseInt(action.currentPage, 10) : 1,
		foundRecord: action.foundRecord >= 0 ? action.foundRecord : 0,
		keyWord: action.keyWord ? action.keyWord : '',
		lat: action.lat ? action.lat : 0,
		lng: action.lng ? action.lng : 0,
		hq: action.hq ? action.hq : '',
	});

const reducers = combineReducers({
	changeFilterType, changeDetailMapType, listBox, focusBox,
});

export default reducers;
